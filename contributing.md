Contributing
===

First and foremost, thank you for stepping in and trying to be part of the effort.

Goals
---

When expanding **Framework in a File**, keep in mind its main priorities:

1. **Maximum PHP version range compatibility**: FnF support and must continue to
   work down to at least PHP 5.3.
2. **Ease of use**: developers must be able to install FnF effortlessly and
   start using it immediately with no additional required steps. It should also
   provide flexible and simple to use configuration when the user feels
   confident enough. Through configuration FnF should be able to adapt to
   popular workflow scenarios.
3. **No feature creep**: requests for edge use cases, especially ones that can
   be worked around with some configuration, should *not* be catered to. The
   definition of edge case is the popularity of the feature request, if it's
   popular - it's a workflow, if not - it might be an edge case.
4. **Code quality**: the nature of FnF implies that it will be continued to
   develop for many years. Keep the codebase as maintainable as possible.
5. **Stable releases**: never publish versions that are not thoroughly tested to
   the master branch.
