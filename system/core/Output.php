<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Output Core Class.
 *
 * @package  DigitalPoetry\FnF\Core
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Output
{
	/**
	 * The page title prefix.
	 *
	 * @var string
	 */
	private $title_prefix = 'FnF | ';

	/**
	 * The page default title.
	 *
	 * @var string
	 */
	private $default_title = 'File in a Framework';

	/**
	 * The page default title.
	 *
	 * @var string
	 */
	private $default_footer = 'Developed by Jesse LaReaux in Arizona.';

	/**
	 * The page title.
	 *
	 * @var string
	 */
	private $title;

	/**
	 * The page header.
	 *
	 * @var string
	 */
	private $header;

	/**
	 * The page content.
	 *
	 * @var string
	 */
	private $content;

	/**
	 * The footer text.
	 *
	 * @var string
	 */
	private $footer;

	/**
	 * The page navigation items.
	 *
	 * @var array
	 */
	private $nav_items = array();

	/**
	 * The page header template part.
	 *
	 * @var string
	 */
	private $template_header = '##THEME_HEADER.HTML##';

	/**
	 * The page styles template part.
	 *
	 * @var array
	 */
	private $styles = array('##THEME_STYLES.HTML##');

	/**
	 * The page scripts template part.
	 *
	 * @var array
	 */
	private $scripts = array('##THEME_SCRIPTS.HTML##');

	/**
	 * The UI Kit helper object.
	 *
	 * @var BootdtrapUIKit
	 */
	public $uikit;

	/**
	 * Adds a navigation item.
	 *
	 * @param string  $id       Menu item id. Must be unique.
	 * @param integer $priority Menu order priority. Lower is a higher priority.
	 * @param string  $title    Title of the menu item.
	 * @param string  $href     Optional. URL of the menu item.
	 * @param array   $submenu  Optional. Submenu item list.
	 * @return void
	 */
	public function add_nav_item($id, $priority, $title, $href = '#', $submenu = array())
	{
		// Add the nav item to the array.
		$this->nav_items[$id] = array(
			'id'       => $id,
			'title'    => $title,
			'href'     => $href,
			'priority' => $priority,
			'submenu'  => $submenu
		);
	}

	/**
	 * Adds a submenu item.
	 *
	 * @param string $id     Parent menu item id.
	 * @param string  $title Title of the menu item.
	 * @param string  $href  URL of the menu item.
	 * @return void
	 */
	public function add_submenu_item($id, $title, $href)
	{
		// Add the nav item to the array.
		$this->nav_items[$id]['submenu'][] = array(
			'title' => $title,
			'href'  => $href
		);
	}

	/**
	 * Sets a submenu.
	 *
	 * Set the submenu for a navigation item. Please note that this will
	 * over-write anything set already.
	 *
	 * @param string $id      Parent menu item id.
	 * @param array  $submenu Submenu item list.
	 * @return void
	 */
	public function set_submenu($id, $submenu)
	{
		// Add the submenu to the nav item.
		$this->nav_items[$id]['submenu'] = $submenu;
	}

	/**
	 * Gets the navigation items.
	 *
	 * Gets the navigation items, sorts them by their menu order priority, and
	 * returns the array of nav items.
	 *
	 * @return array The navigation items.
	 */
	public function get_nav_items()
	{
		// The array of ordered nav items returned.
		$nav_items = [];

		// Sort the nav tems
		foreach ($this->nav_items as $item) {

			// Get the nav item priority.
			$priority = $item['priority'];

			// increment the priority until an open numeric key is found.
			while (array_key_exists($priority, $nav_items)) {
				$priority++;
			}

			// Add the item to the return array.
			$nav_items[$priority] = $item;
		}

		// Sort nav items by array key.
		ksort($nav_items);

		return $nav_items;
	}

	/**
	 * Adds styles to the page.
	 *
	 * @param array $styles The styles to add to the page.
	 */
	public function add_styles($styles)
	{
		$this->styles = array_merge($this->styles, $styles);
	}

	/**
	 * Adds scripts to the page.
	 *
	 * @param array $scripts The scripts to add to the page.
	 */
	public function add_scripts($scripts)
	{
		$this->scripts = array_merge($this->scripts, $scripts);
	}

	/**
	 * Sets The html title tag value.
	 *
	 * @param string $title Required. The string to use in the html title tag.
	 * @return void
	 */
	public function set_title($title)
	{
		// Prefix the title.
		$this->title = $this->title_prefix . $title;
	}

	/**
	 * Gets the html title tag value.
	 *
	 * Gets the page title from the class properties. If the class property is
	 * not set, gets the page title from the active module's class properties.
	 * Returns the default title if a page title is not set anywhere.
	 *
	 * @return string The html title tag value.
	 */
	public function get_title()
	{
		// Return the title from class properties?
		if (isset($this->title)) {
			return $this->title;
		}

		// Bring the app global into scope.
		global $app;

		// Return the title from module properties?
		if (isset($app->module->title)) {
			return $app->module->title;
		}

		// Nothing found, return the default title.
		return $this->default_title;
	}

	/**
	 * Sets the page header.
	 *
	 * @param string $header The page header to display.
	 * @return void
	 */
	public function set_header($header)
	{
		$this->header = $header;
	}

	/**
	 * Gets the page header.
	 *
	 * Gets the page header from the class properties. If the class property is
	 * not set, gets the page header from the active module's class properties.
	 * Returns void if a page header is not set anywhere.
	 *
	 * @return string|void The page header or void of not set.
	 */
	public function get_header()
	{
		// Return the header from class properties?
		if (isset($this->header)) {
			return $this->header;
		}

		// Bring the app global into scope.
		global $app;

		// Return the header from module properties?
		if (isset($app->module->header)) {
			return $app->module->header;
		}
	}

	/**
	 * Sets the page content.
	 *
	 * @param string $content The page content to display.
	 * @return void
	 */
	public function set_content($content)
	{
		$this->content = $content;
	}

	/**
	 * Gets the page content.
	 *
	 * Gets the page content from the class properties. If the class property is
	 * not set, gets the page content from the active module's class properties.
	 * Returns void if a page content is not set anywhere.
	 *
	 * @return string|void The page content or void of not set.
	 */
	public function get_content()
	{
		// Return the content from class properties?
		if (isset($this->content)) {
			return $this->content;
		}

		// Bring the app global into scope.
		global $app;

		// Return the content from module properties?
		if (isset($app->module->content)) {
			return $app->module->content;
		}
	}

	/**
	 * Sets the page footer.
	 *
	 * @param string $footer The page footer to display.
	 * @return void
	 */
	public function set_footer($footer)
	{
		$this->footer = $footer;
	}

	/**
	 * Gets the footer text.
	 *
	 * Gets the footer text from the class properties. If the class property is
	 * not set, gets the footer text from the active module's class properties.
	 * Returns void if a footer text is not set anywhere.
	 *
	 * @return string The footer text.
	 */
	public function get_footer()
	{
		// Return the footer from class properties?
		if (isset($this->footer)) {
			return $this->footer;
		}


		// Bring the $app global into scope.
		global $app;

		// Return the footer from module properties?
		if (isset($app->module->footer)) {
			return $app->module->footer;
		}

		// Nothing found, return the default footer.
		return $this->default_footer;
	}

	/**
	 * Displays the page.
	 *
	 * @param string $output Optional. The page content to display.
	 * @return void
	 */
	public function render($output = '')
	{
		// Instantiate the UI kit.
		$this->uikit = new bootstrapUIKit;

		// Page content.
		$content = empty($output) ? $this->content : $output;

		// Display the page.
		echo
		$this->template_header() .
		$this->template_nav_menu() .
		$this->p(2, '<!-- Content -->') .
		$this->p(2, '<div class="container">') .
		$this->p(3, '<div class="content">') .
		$this->template_heading() .
		$content .
		$this->p(3, '</div><!-- /#content -->') .
		$this->p(2, '</div><!-- /.container -->') .
		$this->template_footer();
	}

	/**
	 * Gets the theme header.
	 *
	 * @return string The theme header html.
	 */
	protected function template_header()
	{
		return
		$this->template_part('template_header') .
		$this->p(2, '<title>' . $this->get_title() . '</title>') .
		$this->template_part('styles') .
		$this->p(1, '</head>') .
		$this->p(1, '<body>');
	}

	/**
	 * Gets the theme navigation menu.
	 *
	 * @return string The theme nav menu html.
	 */
	protected function template_nav_menu()
	{
		// Bring the app global object into scope.
		global $app;

		// Add menu items.
		$module = $app->route->get_active_module();
		$nav_items = $this->get_nav_items();

		// Start the menu.
		$options = [
			'inverse',
			'fixed',
			'id' => 'main_nav',
		];

		// Navbar with callback.
		$output = $this->uikit->navbar($options, function() use ($nav_items, $module) {

			// Bring the app global object into scope.
			global $app;

			// Add the application brand
			$output = $app->output->uikit->navbarBrand($app->getConfig('app', 'name'), $url='#');

			// Nav menu with callback.
			$output .= $app->output->uikit->nav(['class' => 'navbar-nav'], function() use ($nav_items, $module) {

				/*
				 * Add menu items.
				 */

				// Bring the app global object into scope.
				global $app;

				// Iterate each nav item.
				foreach ($nav_items as $item) {

					// Set variables for $item.
					extract($item);
					$uri = '?view=' . strtolower($id);

					// Is active menu item?
					if ($module == $id) {
						$app->output->uikit->setActiveNavItem($title);
					}

					// Has submenu?
					if (empty($submenu)) {

						// Add menu item
						$output[] = $app->output->uikit->navItem($title, $uri, []);

					} else {

						// Nav dropdown with callback.
						$output[] = $app->output->uikit->navDropdown($title, [], function() use ($item, $module) {

							/*
							 * Add submenu items.
							 */

							// Bring the app global object into scope.
							global $app;

							// Get variables from array.
							extract($item);

							// Iterate each submenu item.
							foreach ($submenu as $sub_item) {

								// Set variables for $sub_item.
								extract($sub_item);

								// Set active submenu item.
								/** @todo Add new query params to handle subpages */
								if ($module == $id && strstr($app->request_uri, $href) != false) {

									$app->output->uikit->setActiveNavItem($title);
								}

								$output[] = $app->output->uikit->navItem($title, $href);
							}

							// Return the output.
							return implode('', $output);
						});
					}
				}

				// Return the output.
				return implode('', $output);
			});

			// Return the output.
			return $output;
		});

		// Return the output.
		return $output;
	}

	/**
	 * Gets the page header.
	 *
	 * @return string The page header html.
	 */
	protected function template_heading()
	{
		// Get the page header.
		$header = $this->get_header();

		// Is the page header empty?
		if (!empty($header)) {
			return
			$this->p(1, '<div class="page-header">') .
			$this->p(2, '<h1>' . $header . '</h1>') .
			$this->p(1, '</div>');
		}
	}

	/**
	 * Gets the theme footer.
	 *
	 * @return string The template footer.
	 *
	 * @todo Get the footer as an Asset and post-inject the footer content...
	 */
	protected function template_footer()
	{
		return
		$this->p(2, '<!-- Footer -->') .
		$this->p(2, '<footer class="footer">') .
		$this->p(3, '<div class="container">') .
		$this->p(4, '<p class="text-muted">' . $this->get_footer() . '</p>') .
		$this->p(3, '</div>') .
		$this->p(2, '</footer><!-- /.footer -->') .
		$this->template_part('scripts') .
		$this->p(1, '</body>') .
		$this->p(0, '</html>');
	}

	/**
	 * Gets a template part from a property.
	 *
	 * @param string $template The name of the template part.
	 * @return string
	 */
	protected function template_part($template)
	{
		// Is template set?
		if (!isset($this->$template)) {
			return $this->p(4, "The template part '{$template}' not found.");
		}

		// Get the template part.
		$template = $this->$template;

		// Is an array?
		if (is_array($template)) {

			// Convert arrays to strings.
			foreach ($template as $part)
				$html[] = base64_decode($part);
			return implode("\n", $html);

		} else {

			return base64_decode($template);
		}
	}

	/**
	 * Convert bytes into a human readble string
	 *
	 * @param int|str $bytes    Required. The bytesize.
	 * @param integer $decimals Optional. The number of decimals. Default is 2.
	 * @return string A friendly filesize string.
	 */
	public function human_filesize($bytes, $decimals = 2)
	{
		$sizes = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
		$factor = floor((strlen($bytes) - 1) / 3);
		$size = $bytes == 0 ? "0" : sprintf("%.{$decimals}f", $bytes / pow(1024, $factor));
		return $size . " " . @$sizes[$factor];
	}

	/**
	 * Convert a timestamp into a human readble string.
	 *
	 * @see http://php.net/manual/en/function.date.php.
	 *
	 * @param int|str $timestamp Required. The timestamp to convert.
	 * @param string  $format    Optional. The date format. Default is
	 *                           "M j, Y, g:i a".
	 * @return string A friendly date string.
	 */
	public function human_date($timestamp, $format = "M j, Y, g:i a")
	{
		return date($format, $timestamp);
	}

	/**
	 * Convert file permissions into a human readble string.
	 *
	 * @see http://php.net/manual/en/function.fileperms.php.
	 *
	 * @param int|str $perms Required. The permissions to convert.
	 * @return string A friendly permissions string.
	 */
	public function human_permissions($perms)
	{
		return substr(sprintf('%o', $perms), -4);
	}

	/**
	 * Space string helper.
	 *
	 * Returns a string with the given number of space characters.
	 *
	 * @param integer $spaces Required. The number spaces characters.
	 * @return string Returns a string of space characters.
	 *
	 * @todo Remove this function if not used.
	 */
	private function s($spaces)
	{
		return str_repeat(" ", $spaces);
	}

	/**
	 * Tab string helper.
	 *
	 * Returns a string with the given number of tab characters.
	 *
	 * @param integer $tabs Required. The number tabs characters.
	 * @return string Returns a string of tab characters.
	 *
	 * @todo Remove this function if not used.
	 */
	private function t($tabs)
	{
		return str_repeat("\t", $tabs);
	}

	/**
	 * Pads a string of html.
	 *
	 * Indents and appends a line feed character to the given string.
	 *
	 * @param integer $tabs      Required. The number of tabs to indent the
	 *                           string.
	 * @param string  $string    Required. The string of html text to pad.
	 * @param boolean $use_space Optional. Use space characters instead of tabs.
	 *                           Default is false.
	 * @return string The padded line of html.
	 */
	private function p($tabs, $string, $use_space = false)
	{
		$ws = ($use_space == true) ? " " : "\t";
		return str_repeat($ws, $tabs) . $string . PHP_EOL;
	}

} // Output
