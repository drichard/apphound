<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Bootstrap Core Class.
 *
 * Instantiates the Application Class & module controller classes. Loading the modules
 * could be done by the Application class, however this would cause object
 * recursion whenever a module accesses the $app global object.
 *
 * @package  DigitalPoetry\FnF\Core
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Bootstrap
{
	/**
	 * Loads the Application core class and module controllers.
	 *
	 * @uses Application::initialise() Runs the framework.
	 * @uses Loader::load()            Populates the Loader object with an
	 *                                 object of the given class.
	 */
	function __construct()
	{
		// Instantiate the App Class make it a global.
		global $app;
		$app = new Application;

		// Instantiate Route and Output classes.
		$app->route = new Route();
		$app->output  = new Output();

		// Instantiate Loader classes.
		$app->helpers   = new Loader();
		$app->libraries = new Loader();
		$app->modules   = new Loader();

		// Instantiate Debug helper class.
		$app->helpers->load('Debug');

		// Start up the application.
		$app->initialise();

		// Autoload modules.
		foreach (get_declared_classes() as $class) {

			// Does the class extend the Controller base class?
			if (is_subclass_of($class, 'Controller')) {

				// Load the class.
				$app->modules->load($class);
			}
		}

		// Run the application.
		$app->run();
	}

} // Bootstrap
