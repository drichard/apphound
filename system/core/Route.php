<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Route Core Class.
 *
 * @package  DigitalPoetry\FnF\Core
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Route
{
	/**
	 * The default module to display. Should be the lowercased name of the
	 * module's controller class.
	 *
	 * @var string
	 */
	public $default_module;

	/**
	 * The module method to call. Should be the name of a module's
	 * controller class method.
	 *
	 * @var string
	 */
	public $method;

	/**
	 * The name of the active module.
	 *
	 * @var string
	 */
	public $module;

	/**
	 * Routes Ajax requests and form posts.
	 *
	 * @return mixed
	 */
	function request()
	{
		// Is there a post?
		if ($_POST) {

			global $app;

			// Validate nonce token
			if ( !isset($_POST['secure_token']) || $_POST['secure_token'] === $app->secure_token ) {

				// Missing or invalid secure token.
				die('Invalid request.');
			}
		}

		// Set the active module.
		$this->set_module();

		// Set the active method.
		$this->set_method();
	}

	/**
	 * Sets the requested module. 
	 *
	 * @param string $module The module controller classname.
	 * @return void
	 */
	public function set_module()
	{
		global $app;

		// Get the query parameter names and default module and method.
		$queryparam     = $app->getConfig('queryparam.module', 'system');
		$default_module = $app->getConfig('default_module', 'system');

		// Use the requested or default module?
		$module = isset($_REQUEST[$queryparam]) ? $_REQUEST[$queryparam] : $default_module;

		// Set the active module name.
		$this->module = $module;

		// Assign module to the active module property.
		$app->module = &$app->modules->$module;
	}

	/**
	 * Sets the requested method.
	 *
	 * @param string $method The methodname.
	 * @return void
	 */
	public function set_method()
	{
		global $app;

		// Get the query parameter names and default module and method.
		$queryparam = $app->getConfig('queryparam.method', 'system');

		// Use the requested method or a default method?
		if (isset($_REQUEST[$queryparam])) {

			$this->method = $_REQUEST[$queryparam];

		} elseif (isset($app->module->default_method)) {

			$this->method = $app->module->default_method;

		} else {

			$this->method = 'index';
		}
	}

	/**
	 * Gets the requested module.
	 *
	 * @return string The requested module classname.
	 */
	public function get_active_module()
	{
		return $this->module;
	}

	/**
	 * Gets the requested module method.
	 *
	 * @return string The requested module method.
	 */
	public function get_active_method()
	{
		return $this->method;
	}

} // Route
