<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Qux
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Qux Module Class Example.
 *
 * Description of your module, what it does and how to use it, goes here...
 * The Qux module example uses the Baz Utility example. Modules must extend the
 * Controller base class or the module will not be loaded.
 *
 * @package  DigitalPoetry\FnF\Controller
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Qux extends Controller
{
	/**
	 * The page title tag.
	 *
	 * @var string
	 */
	public $title = 'Qux Module';

	/**
	 * The page header.
	 *
	 * @var string
	 */
	public $header = 'Qux Module Skeleton Example';

	/**
	 * Registers the module. Loads dependencies.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Add items to menu.
		$this->add_nav_item(20, 'Qux');
	}

	/**
	 * Runs the module. Displays the output.
	 *
	 * @param array $post The http post if one was received.
	 * @return void
	 */
	public function index($post = null)
	{
		/**
		 * Bring the $app global object into scope.
		 *
		 * @global Application.
		 */
		global $app;

		// Output.
		$output = '<p>This is the Qux module. The Qux module is a skeleton example, ' .
		'incase you would rather get started with that. Check out the source ' .
		'code <a href="https://gitlab.com/jlareaux/fnf" ' .
		'title="Framework in a File repository">here</a>.</p>##QUX_BUTTONS_BLOCK.PHP##';

		// Page Content.
		$app->output->set_content($output);

		// Render page.
		$app->output->render();
	}

} // Qux
