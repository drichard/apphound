<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Libraries
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Baz Utility Class Example.
 *
 * @package  DigitalPoetry\FnF\Utility
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Baz
{
	/**
	 * Outputs the utility.
	 *
	 * @return string  The utility output.
	 */
	public function output()
	{
		// Bring the $app global into scope.
		global $app;

		// Print the $app global object.
		$output ='<pre>' . print_r($app, true) . '</pre>';

		// Return the output.
		return $output;
	}

} // Baz
