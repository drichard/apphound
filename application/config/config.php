<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Config
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * The application name.
 */
$config['app']['name'] = 'Framework in a File';

/**
 * The application nickname.
 */
$config['app']['nickname'] = 'FnF';

/**
 * The application url. Optional.
 */
$config['app']['url'] = '';

/**
 * The application admin email.
 */
# $config['app']['notifications_email'] = 'jlareaux@gmail.com';
