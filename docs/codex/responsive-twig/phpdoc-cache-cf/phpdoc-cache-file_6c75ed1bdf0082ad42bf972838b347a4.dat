O:39:"phpDocumentor\Descriptor\FileDescriptor":22:{s:7:" * hash";s:32:"f88cd394330da7ef3edf1a6fab8f35c3";s:7:" * path";s:26:"build\config_structure.php";s:9:" * source";s:5897:"<?php
/**
 * FnF Structure Configuration
 *
 * Configure the paths to the source, assets and project folders.
 *
 * NOTE: Paths MUST not have leading or trailing slashes.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/*
 * Project directories
 *
 * NOTE: Project paths MUST be relative from the project root directory.
 */
$config['paths']['project'] =
[
	/*
	 * The application directory path. Contains source files used to
	 * compile the application.
	 */
	'application' => 'application',

	/*
	 * The build directory path. Contains the scripts used to compile the
	 * framework. If this path is changed, build_run.php must be edited as well.
	 */
	'build' => 'build',

	/*
	 * The documention directory path. Contains any auto-generated or
	 * hand-written documention about your project or third party libraries.
	 */
	'docs' => 'docs',

	/*
	 * The project system directory path. Contains source files used to compile
	 * the application.
	 */
	'system' => 'system',

	/*
	 * The project tests directory path. Contains the automated tests used when
	 * building the application.
	 */
	'tests' => 'tests',

	/*
	 * The theme directory path. Contains source files used to compile the
	 * application.
	 */
	'theme' => 'theme',

	/*
	 * The temporary directory path. Contains temporary files used by FnF to
	 * compile and build the application.
	 */
	'temp' => 'temp',

	/*
	 * The Vendor directory path. Contains any third party libraries used during
	 * development only.
	 *
	 * Note: This directory will not be included in the compiled application.
	 */
	'vendor' => 'vendor'
];

/*
 * Source directories
 *
 * These directories are scanned at build time for source files to include in
 * the compiled application. Any file found in one of the below directories will
 * be complied and included in ths application at build time. You can add to
 * this array and your directory will be automagically included in thhe compiled
 * application.
 *
 * NOTE: Application source paths MUST be relative from the project application
 * directory.
 *
 * NOTE: System source paths MUST be relative from the project root directory, and
 * MUST have a path inside of the Project System directory..
 */
$config['paths']['sources']['app'] =
[
	/*
	 * The application configuration directory path.
	 */
	'config' => 'config',

	/*
	 * The application core classes directory path.
	 */
	'core' => 'core',

	/*
	 * The application controllers directory path.
	 */
	'controllers' => 'controllers',

	/*
	 * The application helpers directory path.
	 */
	'helpers' => 'helpers',

	/*
	 * The application libraries directory path.
	 */
	'libraries' => 'libraries',

	/*
	 * The application modules directory path.
	 */
	'modules' => 'modules',

	/*
	 * The third party libraries directory path.
	 */
	'third-party' => 'third_party'
];

$config['paths']['sources']['system'] =
[
	/*
	 * The FnF configuration directory path.
	 */
	'config' => 'system' . DS . 'config',

	/*
	 * The FnF core classes directory path.
	 */
	'core' => 'system' . DS . 'core',

	/*
	 * The FnF file parts directory path.
	 */
	'file' => 'system',

	/*
	 * The FnF helpers directory path.
	 */
	'helpers' => 'system' . DS . 'helpers',

	/*
	 * The FnF libraries directory path.
	 */
	'libraries' => 'system' . DS . 'libraries'
];

/*
 * Asset directories
 *
 * These directories are scanned at build time for assets to embed in the
 * compiled source code.
 *
 * NOTE: Application asset paths MUST be relative from the project application
 * directory path.
 *
 * NOTE: Theme asset paths MUST be relative from the relative from the project root
 * directory, and MUST have a path inside of the project theme directory.
 */
$config['paths']['assets']['app'] =
[
	/*
	 * The application css directory path.
	 */
	'css'   => 'assets' . DS . 'css',

	/*
	 * The application image directory path.
	 */
	'img'   => 'assets' . DS . 'img',

	/*
	 * The application javascript directory path.
	 */
	'js'    => 'assets' . DS . 'js',

	/*
	 * The application view directory path.
	 */
	'views' => 'views'
];

$config['paths']['assets']['theme'] =
[
	/*
	 * The theme css directory path.
	 */
	'css' => 'theme' . DS . 'css',

	/*
	 * The theme image directory path.
	 */
	'img' => 'theme' . DS . 'img',

	/*
	 * The theme javascript directory path.
	 */
	'js' => 'theme' . DS . 'js',

	/*
	 * The theme views directory path.
	 */
	'views' => 'theme'
];

/*
 * Temporary directories
 *
 * These directories contain tempory or cached data. Do not put files you want
 * to keep in these dirrectories as they may be deleted.
 *
 * NOTE: Temporary paths MUST be relative from the project root directory.
 */
$config['paths']['temp'] =
[
	/*
	 * The cache directory path. Contains temporary and cache files for builds.
	 */
	'cache' => 'temp' . DS . 'cache',

	/*
	 * The docs cache directory path. Contains cache files for documentation
	 * generation.
	 */
	'docs' => 'temp' . DS . 'docs',

	/*
	 * The live run directory path. Contains temporary files used to run the
	 * application in live mode.
	 */
	'live'  => 'temp' . DS . 'live',

	/*
	 * The log directory path. Contains logs generated during builds.
	 */
	'logs'  => 'temp' . DS . 'logs',

	/*
	 * The Release directory path. Contains files resulting from release builds.
	 */
	'release'  => 'temp' . DS . 'release'
];
";s:19:" * namespaceAliases";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:11:" * includes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * functions";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * classes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:13:" * interfaces";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * traits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * markers";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:0:"";s:7:" * name";s:20:"config_structure.php";s:12:" * namespace";N;s:10:" * package";s:23:"DigitalPoetry\FnF\Build";s:10:" * summary";s:27:"FnF Structure Configuration";s:14:" * description";s:118:"Configure the paths to the source, assets and project folders.

NOTE: Paths MUST not have leading or trailing slashes.";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:9:{s:7:"package";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"package";s:14:" * description";s:23:"DigitalPoetry\FnF\Build";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"author";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\AuthorDescriptor":3:{s:7:" * name";s:6:"author";s:14:" * description";s:34:"Jesse LaReaux <jlareaux@gmail.com>";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:9:"copyright";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:9:"copyright";s:14:" * description";s:61:"Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:7:"license";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"license";s:14:" * description";s:46:"MIT License http://opensource.org/licenses/MIT";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:7:"version";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Tag\VersionDescriptor":4:{s:10:" * version";s:5:"0.1.0";s:7:" * name";s:7:"version";s:14:" * description";s:12:"Basic Things";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"since";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\SinceDescriptor":4:{s:10:" * version";s:5:"0.1.0";s:7:" * name";s:5:"since";s:14:" * description";s:12:"Basic Things";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:4:"link";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:43:"phpDocumentor\Descriptor\Tag\LinkDescriptor":4:{s:7:" * link";s:31:"https://gitlab.com/jlareaux/fnf";s:7:" * name";s:4:"link";s:14:" * description";s:31:"https://gitlab.com/jlareaux/fnf";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"filesource";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:10:"filesource";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}