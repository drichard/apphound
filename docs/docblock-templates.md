
DocBlock Templates
====================

TODO:
- [ ] Add annotations to DocBlock Templates

****************************************

#### Single Line Comments

Not a DocBlock:
```php
// Allow plugins to filter an array.
```

DocBlock:
```php
/** Allow plugins to filter an array. */
```

****************************************

#### Multi-line Comments

Not a DocBlock:
```php
/*
 * This is a comment that is long enough to warrant being stretched over
 * the span of multiple lines. You'll notice this follows basically
 * the same format as the PHPDoc wrapping and comment block style.
 */
```

DocBlock:
```php
/**
 * This is a comment that is long enough to warrant being stretched over
 * the span of multiple lines. You'll notice this follows basically
 * the same format as the PHPDoc wrapping and comment block style.
 */
```

****************************************

#### Markers

Mark code for improvements:
```php
/** TODO: Description. */
```

Mark code for bugs:
```php
/** FIXME: Description. */
```

****************************************

#### File

Standard Usage:
```php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\FnF
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license     MIT License http://opensource.org/licenses/MIT
 * @link        <link>
 * @version     <version> [<releasename>] [<checksum>]
 * @filesource
 */
```

Complete Usage:
```php
/**
 * Summary (no period for file headers)
 *
 * Description.
 *
 * @package     DigitalPoetry\FnF
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license     MIT License http://opensource.org/licenses/MIT
 * @version		<version> [<releasename>] [<checksum>]
 * @since		<version> [<releasename>] [<checksum>]
 * @link        <link>
 * @filesource
 *
 * @todo        Description.
 */
```

****************************************

#### Require(_once), Include(_once)

Standard Usage:
```php
/**
 * Summary.
 *
 * @since	<version> [<releasename>] [<checksum>]
 * @see     <uri>|<fqsen> [Description.]
 * @link    <link>
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @since	<version> [<releasename>] [<checksum>]
 * @see     <uri>|<fqsen> [Description.]
 * @link	<link>
 * @todo	Description.
 */
```

****************************************

#### Class

Standard Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @since	<version> [<releasename>] [<checksum>]
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @author		Jesse LaReaux <jlareaux@gmail.com>
 * @copyright	Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license		MIT License http://opensource.org/licenses/MIT
 *
 * @package		CATT
 * @version     <version> [<releasename>] [<checksum>]
 * @since		<version> [<releasename>] [<checksum>]
 * @deprecated	<version> [Description.]
 * @see			<fqsen>.
 *
 * @uses		<fqsen> [Description.]
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length> [Description.]
 * @todo		Description.
 */
```

****************************************

#### Function

Standard Usage:
```php
/**
 * Summary.
 *
 * Description.
 *
 * @version	<version> [<releasename>] [<checksum>]
 * @since   <version> [<releasename>] [<checksum>]
 *
 * @global	<type> $var [Description.]
 * @uses	<fqsen> [Description.]
 * @see		<url>|<fqsen> [Description.]
 * @link	<link>
 *
 * @param	<type> $var [Description.]
 * @return	<type> Description.
 * @throws	<type> Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description.
 *
 * @version		<vector> Description
 * @since       <version> [<releasename>] [<checksum>]
 * @deprecated	<version> [Description.]
 * @see			[Description.]
 *
 * @global		<type> $var [Description.]
 * @uses		<fqsen> [Description.]
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length>
 * @todo		Description.
 *
 * @param	<type> $var [Description.]
 * @return	<type> Description.
 * @throws	<type> Description.
 */
```

****************************************

#### Method

Standard Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @api
 * @since   <version> [<releasename>] [<checksum>]
 *
 * @global	<type> $var [Description.]
 * @uses	<fqsen> [Description.]
 * @see		<url>|<fqsen> [Description.]
 * @link	<link>
 *
 * @param	<type> $var [Description.]
 * @return	<type> Description.
 * @throws	<type> Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @api
 * @since       <version> [<releasename>] [<checksum>]
 * @deprecated	<version> [Description.]
 * @see			[Description.]
 *
 * @global		<type> $var [Description.]
 * @uses		<fqsen> [Description.]
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length>
 * @todo		Description.
 *
 * @param	<type> $var [Description.]
 * @return	<type> Description.
 * @throws	<type> Description.
 */
```

****************************************

#### Property

Standard Usage:
```php
/**
 * Summary, {@inheritdoc}.
 *
 * @inheritdoc
 * @version	<version> [<releasename>] [<checksum>]
 * @since   <version> [<releasename>] [<checksum>]
 * @var		<type> $var [Description.]
 */
```

Complete Usage:
```php
/**
 * Summary, {@inheritdoc}.
 *
 * @inheritdoc
 * @version		<version> [<releasename>] [<checksum>]
 * @since       <version> [<releasename>] [<checksum>]
 * @deprecated	<version> [Description.]
 * @see			<fqsen> [Description.]
 *
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length> [Description.]
 * @todo		Description.
 *
 * @var			<type> $var [Description.]
 */
```

****************************************

#### [class] Constant:

Standard Usage:
```php
/**
 * Summary.
 *
 * @version	<version> [<releasename>] [<checksum>]
 * @since   <version> [<releasename>] [<checksum>]
 * @var		<type> $var [Description.]
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @inheritdoc
 * @version		<version> [<releasename>] [<checksum>]
 * @since       <version> [<releasename>] [<checksum>]
 * @deprecated	<version> [Description.]
 * @see			<fqsen> [Description.]
 *
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length> [Description.]
 * @todo		Description.
 *
 * @var			<type> $var [Description.]
 */
```

****************************************

#### Global Variable

Standard Usage:
```php
/**
 * Summary.
 *
 * @version	<version> [<releasename>] [<checksum>]
 * @since   <version> [<releasename>] [<checksum>]
 * @global	<type> Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @version		<version> [<releasename>] [<checksum>]
 * @since       <version> [<releasename>] [<checksum>]
 * @deprecated	<version> Use new_global_name().
 * @see			<fqsen> [Description.]
 *
 * @global 		<type> Description.
 * @see			<url>|<fqsen> [Description.]
 * @example		<location> <start> <length> [Description.]
 * @link		<link>
 * @source		<start> <length> [Description.]
 * @todo		Description.
 */
```

****************************************

#### Param, <type>, Property, Etc

```php
/**
 * @param string   $var  Optional. The _ string.  Default _.
 * @param integer  $var  Optional. The number of _. Default _.
 * @param float    $var  Optional. The value of _. Default _.
 * @param number   $var  Optional. The _ number. Default _.
 * @param boolean  $var  Optional. _ if set to true, _ if false. Default _.
 * @param array    $var  Optional. An array of _. Default _.
 * @param object   $var  Optional. The _ object. Default _.
 * @param static   $var  Optional. The _ instance. Default _.
 * @param mixed    $var  Optional. The value of _. Default _.
 * @param null     $var  Optional. An empty value. Default _.
 * @param resource $var  Optional. The _ file handler. Default _.
 * @param callable $var  Optional. The _ handler. Default _.
 * @param int|str  $var  Optional. Description. Default _.
 * @param string[] $var  Optional. An array of _ strings. Default _.
 * @param array[]  $var  Optional. An array of _ arrays. Default _.
 * @param object[] $var  Optional. An array of _ objects. Default _.
 * @param mixed[]  $var  Optional. An array of _ values. Default _.
 * @param array    args {
 *      Optional. An array of _. Default _.
 *      @<type> <type>  $key  Optional. Accepts 'value', 'value'. (align with
 *                            Description if wraps to a new line) Default _.
 *      @<type> string  $key  Optional. The _ string. Default _.
 *      @<type> integer $key  Optional. he number of _. Default _.
 *      @<type> object  $key  Optional. The _ object. Default _.
 *      @<type> boolean $key  Optional. _ if set to true, _ if false. Default _.
 * }
 */
```

****************************************

References:
- [phpDoc Documentation](https://www.phpdoc.org/docs/latest/index.html)
- [PSR-5: PHPDoc Standard](https://github.com/phpDocumentor/fig-standards/tree/master/proposed)
- [PSR-?: PHPDoc Standard Revision](https://github.com/mvriel/phpDocumentor2/blob/b803b549189926ae6880cabd699675f22d8aade1/docs/PSR.md)
- [PhpDoc2Cheatsheet](http://phpdoc2cheatsheet.com/)
- [WordPress PHP Documentation Standards](https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/php/#resources)
