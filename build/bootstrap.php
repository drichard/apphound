<?php
/**
 * FnF Build Boostrap
 *
 * Defines constants and includes files used during builds.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Ensure error reporting is on.
ini_set("error_reporting", "true");
ini_set('display_errors', "true");
error_reporting(E_ALL|E_STRCT);

// Define directory separator.
defined('DS') OR define('DS', DIRECTORY_SEPARATOR);

// Build config files.
require_once 'config_build.php';
require_once 'config_structure.php';

// Bring $config into scope.
global $config;

// Define Paths.
defined('BASEPATH')    OR define('BASEPATH', $config['build']['project_root']);
defined('SCRIPTSPATH') OR define('SCRIPTSPATH', $config['paths']['project']['build'] . DS . 'scripts');

// Application & System config files.
require_once BASEPATH . DS . $config['paths']['project']['application'] . DS . $config['paths']['sources']['app']['config'] . DS . 'config.php';
require_once BASEPATH . DS . $config['paths']['sources']['system']['config'] . DS . 'config.php';

// Build classes.
require_once 'lib' . DS . 'BaseBuilder.php';
require_once 'lib' . DS . 'BuildApp.php';
require_once 'lib' . DS . 'BuildProject.php';
require_once 'lib' . DS . 'BuildRelease.php';

// Composer autoload.
if( file_exists(BASEPATH . DS . $config['paths']['project']['vendor'] . DS . 'autoload.php') )
	include_once BASEPATH . DS . $config['paths']['project']['vendor'] . DS . 'autoload.php';

// Debug helper functions.
require_once BASEPATH . DS . 'debug.php';
