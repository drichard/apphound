<?php
/**
 * FnF Release Builder
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * The BuildRelease Class
 *
 * @todo Consider removing the 'basename(BASEPATH)' part of the release path.
 *
 * @package  DigitalPoetry\FnF\Build
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class BuildRelease extends BaseBuilder
{
	/**
	 * Compiles a release of the application.
	 *
	 * @return void
	 */
	public function release()
	{
		//$src_dir       = BASEPATH;
		//$app_dir       = $this->paths['project']['application'];
		//$build_dir     = $this->paths['project']['build'];
		$docs_dir      = $this->paths['project']['docs'];
		//$tests_dir     = $this->paths['project']['tests'];
		$temp_dir      = $this->paths['project']['temp'];
		$vend_dir      = $this->paths['project']['vendor'];
		$release_dir   = $this->paths['temp']['release'];
		$release_path  = BASEPATH . DS . $release_dir . DS . $this->docblock['version'];
		$script        = BASEPATH . DS . $this->script_path . DS . $this->script_name;
		$version_strs  = [$this->docblock['version'], $this->docblock['releasename'], $this->docblock['checksum']];
		$replace       = '$1 ' . implode(' ', $version_strs);
		$find          = [
			'/(@version\s*)\s.*/',
			'/(@since\s*)\s[^\d\s].*/'
		];

		// Set path to release root.
		$path = $release_path . DS . basename(BASEPATH);

		// Delete any release folder contents.
		@$this->cleanFolder($path);

		// Copy Project root, ignore the temp & vendor folders.
		$ignore = [$temp_dir, $vend_dir];
		$this->copyFolder(BASEPATH, $path, $ignore);

		// Create a backup archive.
		$file = $release_path . DS  . 'pre_release_backup_' . date("n-j-y_h-i-s_a") . '.zip';
		$this->compressFolder($path, $file);

		// Find and replace version tags
		$ignore = [$docs_dir];
		$this->findReplace($find, $replace, $path, true, $ignore);
		$this->findReplace($find, $replace, $script, true);
	}

	/**
	 * Compiles a release of the framework.
	 *
	 * @return void
	 */
	public function releaseFnF()
	{
		$release_dir  = $this->paths['temp']['release'];
		$release_path = BASEPATH . DS . $release_dir . DS . $this->docblock['version'];
		$path         = $release_path . DS . basename(BASEPATH);

		// Find and replace version in composer config.
		$file    = $path . DS . 'composer.json';
		// Find "version": "..."
		$find    = '/"version": ".+?"/';
		$replace = '"version": "' . $this->docblock['version'] . '"';
		$this->findReplace($find, $replace, $file, true);

		// Remove salt.
		$file = $path . DS  . $this->paths['sources']['system']['config'] . DS . 'config.php';
		// Find 'secure_salt' = '...'; or "secure_salt" = "...";
		$find = '/\[[\'\"]secure_salt[\'\"]\] = [\'\"].+?[\'\"];/';
		$replace = "['secure_salt'] = '<salt_me>';";
		$this->findReplace($find, $replace, $file, true);

		// Install missing folders.
		$builder = new BuildProject();
		$builder->install($path);

		// Put a gitkeep file in empty folders.
		$this->gitkeepEmpty($path);

		// Create a release archive.
		$file = $release_path . DS  .'File_in_a_Framework_' . $this->docblock['version'] . '.zip';
		$this->compressFolder($path, $file);

		// Add new salt.
		$project_builder = new BuildProject;
		$file = $path . DS  . $this->paths['sources']['system']['config'] . DS . 'config.php';
		$replace = "['secure_salt'] = '" . $project_builder->generateSalt() . "';";
		$this->findReplace($find, $replace, $file, true);
	}

	/**
	 * Compiles a release of the framework.
	 *
	 * @return void
	 */
	public function move_to_root()
	{
		$src_dir      = BASEPATH;
		$release_dir  = $this->paths['temp']['release'];
		$release_path = BASEPATH . DS . $release_dir . DS . $this->docblock['version'];
		$path         = $release_path . DS . basename(BASEPATH);
		$ignore       = ['composer.json'];

		// Move folder. Copy and delete so .gitkeep files are not moved.
		$this->copyFolder($path, $src_dir, $ignore);
		$this->cleanFolder($path);
		rmdir($path);
	}

} // BuildRelease
