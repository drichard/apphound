<?php
/**
 * FnF Base Builder
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * The BaseBuilder Class.
 *
 * Provides common helper functions to builder classes.
 *
 * @package  DigitalPoetry\FnF\Build
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class BaseBuilder
{
	/**
	 * The compiled application filename. Set in the build configuration.
	 *
	 * @var array
	 */
	public $script_name;

	/**
	 * The compiled application filepath. Set in the build configuration.
	 *
	 * @var array
	 */
	public $script_path;

	/**
	 * List of project paths. Set in the build configuration.
	 *
	 * @var array
	 */
	public $paths;

	/**
	 * List of DocBlock tag values. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $docblock;

	/**
	 * Web address to the application directory. No trailing slashes. Set in the
	 * build configuration.
	 *
	 * @var string
	 */
	protected $url;

	/**
	 * The cache type to use when building. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $cache_options;

	/**
	 * Holds the working source lists. Set in the build configuration.
	 *
	 * @var array
	 */
	public $file_lists;

	/**
	 * Holds the working asset lists.
	 *
	 * @var array
	 */
	protected $asset_lists;

	/**
	 * Holds the working image lists.
	 *
	 * @var array
	 */
	protected $image_lists;

	/**
	 * List of sources to build from cache. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $cache_file_list;

	/**
	 * List of files to ignore during builds. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $ignore_files;

	/**
	 * List of folders to ignore during builds. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $ignore_folders;

	/**
	 * List of files to leave when deleting. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $leave_files;

	/**
	 * List of folders to leave when deleting. Set in the build configuration.
	 *
	 * @var array
	 */
	protected $leave_folders;

	/**
	 * The 4 digit permissions to use when creating new directories. Set in the
	 * build configuration.
	 *
	 * @var array
	 */
	protected $default_folder_perms;

	/**
	 * Initialises the build script.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Set the base builder properties.
		$this->setBaseProperties();
	}

	/**
	 * Initialises the build script.
	 *
	 * @return void
	 */
	protected function setBaseProperties()
	{
		// Basic build settings.
		global $config;
		$relative_nodes       = ['.', '..'];
		$this->script_name    = $config['build']['script_name'];
		$this->script_path    = $config['build']['script_path'];
		$this->url            = $config['build']['url'];
		$this->cache_options  = $config['build']['cache_options'];
		$this->file_lists     = $config['build']['files'];
		$this->ignore_files   = $config['build']['ignore_files'];
		$this->leave_files    = $config['build']['leave_files'];
		$this->ignore_folders = array_merge($relative_nodes, $config['build']['ignore_folders']);
		$this->leave_folders  = array_merge($relative_nodes, $config['build']['leave_folders']);
		$this->folder_perms   = $config['build']['default_folder_perms'];
		$this->docblock       = $config['build']['docblock'];
		$this->paths          = $config['paths'];
	}

	/**
	 * Creates the folder if it doesn't already exist.
	 *
	 * @param string $path The folder to create.
	 * @return boolean Returns true if the folder exists or was created, false
	 *                 if not.
	 * @return boolean
	 */
	public function ensureFolder($path)
	{
		// Does the directory exist?
		if (is_dir($path)) {
			return true;
		}
		// Create the folder.
		return mkdir($path, $this->folder_perms, true);
	}

	/**
	 * Deletes folder contents.
	 *
	 * Deletes all files and folders, recursively, within $path, except for any
	 * files or folders listed in the $leave_files or $leave_folders properties.
	 *
	 * @param string $path        The path to the directory to empty.
	 * @param array  $leave_files Optional. An array of filenames to not delete.
	 * @return void
	 */
	public function cleanFolder($path, $leave_files = [])
	{
		// Exclude folders and files.
		$leave_all = array_merge($leave_files, $this->leave_files,  $this->leave_folders);

		// Get directory conents.
		$nodes = array_diff(scandir($path), $leave_all);

		foreach ($nodes as $node) {
			$node = $path . DS . $node;
			if (is_dir($node)) {

				// Delete folder contents recursively.
				$this->cleanFolder($node, $leave_files);
				if (@rmdir($node) === false) {
					$this->abort("Unable to delete '{$node}'");
				}
			} else {
				if (@unlink($node) === false) {
					$this->abort("Unable to delete '{$node}'");
				}
			}
		}
	}

	/**
	 * Copy folder contents.
	 *
	 * Copies the entire contents of a single source folder into a destination
	 * folder, recursively.
	 *
	 * @param string $src   The source folder path.
	 * @param string $dest  The destination folder path.
	 * @param array  $ignore Optional. Array of folder and file names to skip.
	 * @return void
	 */
	public function copyFolder($src, $dest, $ignore = [])
	{
		// Does the destination exist?
		if ($this->ensureFolder($dest) === false) {
			$this->abort("Unable to create folder '{$dest}'.");
		}

		// Excluded file and folders.
		$ignore = array_merge($ignore, $this->ignore_files, $this->ignore_folders);

		// Open the directory.
		$handle = opendir($src);

		// Iterate directory contents.
		while (false !== ( $node = readdir($handle)) )
		{
			// Skip excluded file and folders.
			if (in_array($node, $ignore)) {
				continue;
			}

			// Set paths.
			$src_path  = $src  . DS . $node;
			$dest_path = $dest . DS . $node;

			// Is a directory?
			if (is_dir($src_path)) {

				// Recurse into directories.
				$this->copyFolder($src_path, $dest_path, $ignore);

			// Was the file copied?
			} elseif (! copy($src_path, $dest_path)) {
				$this->abort("Unable to copy from '{$src_path}' to '{$dest_path}'.");
			}
		}

		// Close the directory.
		closedir($handle);
	}

	/**
	 * Compress a directory into an archive.
	 *
	 * @param string   $src          The archive source path.
	 * @param string   $dest         The archive file destination path.
	 * @param boolean  $include_dir  Optional. Set true to include the taget
	 *                               directory in the archive. Default is false.
	 * @param array    $ignore       Optional. Array of folder and file names to
	 *                               skip. Folders must be empty to be ignored.
	 * @return boolean Returns true on success or false on failure.
	 */
	public function compressFolder($src, $dest, $include_dir=false, $ignore = [])
	{
		// Is the Zip extension loaded?
		if (extension_loaded('zip') === false) {
			$this->abort('ZipArchive extension is required.');
		}

		// Does the source exist?
		if (file_exists($src) === false) {
			$this->abort('Source folder not found for zipping.');
		}

		// Does the destination exist?
		if (file_exists($dest)) {

			// Delete the zip if it exists already.
			if (unlink($dest) === false) {
				$this->abort("Unable to delete '{$dest}'.");
			}
		}

		// Open the zip archive.
		$zip = new ZipArchive();
		if (! $zip->open($dest, ZIPARCHIVE::CREATE)) {
			$this->abort('Unknown error opening zip file.');
		}

		// Get real path and replace directory seperators.
		$src = str_replace('\\', '/', realpath($src));

		// Archive a file or folder?
		if (is_dir($src) === true) {

			// Get the folder contents.
			$nodes = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($src), RecursiveIteratorIterator::SELF_FIRST);

			// Include the source folder?
			if ($include_dir) {

				// Add folder to archive.
				$zip->addEmptyDir(basename($src));

				// Remove the main folder from the path.
				$src = dirname($src);
			}

			// Excluded file and folders.
			$ignore = array_merge($ignore, $this->ignore_files, $this->ignore_folders);

			// Add files and folders to archive.
			foreach ($nodes as $node) {

				// Skip excluded file and folders.
				if (in_array($node, $ignore)) {
					continue;
				}

				// Get real and relative paths and replace directory seperators.
				$path     = str_replace('\\', '/', realpath($node));
				$rel_path = str_replace($src . '/', '', $path);

				// Is a file or folder?
				if (is_dir($path) === true) {

					// Add folder to archive.
					$zip->addEmptyDir($rel_path . '/');

				} elseif (is_file($path) === true) {

					// Add file to archive.
					$zip->addFromString($rel_path, file_get_contents($path));
				}
			}

		} elseif (is_file($src) === true) {

			// Add file to archive.
			$zip->addFromString(basename($src), file_get_contents($src));
		}

		// Close the zip archive.
		return $zip->close();
	}

	/**
	 * Lists the folders contained inside the specified path.
	 *
	 * @param string $path  Path to scan for folders.
	 * @return array Folder names found inside the specified path.
	 */
	public function listFolders($path)
	{
		$folders = [];
		$path   = BASEPATH . DS . $path;

		// Get list of nodes.
		$nodes = $this->scanFolder($path);

		foreach ($nodes as $node) {
			if (is_dir($path . DS . $node)) {
				$folders[] = $node;
			}
		}

		return $folders;
	}

	/**
	 * Lists the files contained inside the specified path.
	 *
	 * @param string $path  Path to scan for files.
	 * @return array Filenames found inside the specified path.
	 */
	public function listFiles($path)
	{
		$files = [];
		$path = BASEPATH . DS . $path;

		// Get list of nodes.
		$nodes = $this->scanFolder($path);

		foreach ($nodes as $node) {
			if (is_file($path . DS . $node)) {
				$files[] = $node;
			}
		}

		return $files;
	}

	/**
	 * Lists the nodes contained in a given folder. Does not return files and
	 * folders in the ignore lists.
	 *
	 * @param string $path  Path to scan for nodess.
	 * @return array List of nodenames found inside the specified path.
	 */
	public function scanFolder($path)
	{
		if (! file_exists($path)) {
			return [];
		}

		// Scan and remove nodes from ignore list.
		return array_diff(scandir($path), $this->ignore_files, $this->ignore_folders);
	}

	/**
	 * Finds and replaces a string in a file or folder.
	 *
	 * Searches files and folders recursively for a string to replace.
	 *
	 * @param str|arr  $find     The string to search for.
	 * @param str|arr  $replace  The replacement string.
	 * @param string   $where    The path to the file or folder to search in.
	 * @param boolean  $regex    Weither to preform a regex search or not.
	 * @param array    $ignore   Optional. An array of folder and file names to
	 *                           not copy.
	 * @return void
	 */
	public function findReplace($find, $replace, $where, $regex = false, $ignore = [])
	{
		// Is a file or folder?
		if (is_file($where)) {

			// Get the file contents
			$contents = file_get_contents($where);

			// Find & replace file contents.
			if ($regex) {
				$contents = preg_replace($find, $replace, $contents);
			} else {
				$contents = str_replace($find, $replace, $contents);
			}

			// Write file contents.
			if (file_put_contents($where, $contents) === false) {
				die("Unable to write to file '{$where}'.");
			}

		} elseif (is_dir($where)) {

			// Excluded file and folders.
			$ignore = array_merge($ignore, $this->ignore_files, $this->ignore_folders);

			// Open the folder.
			if (false === ($handle = opendir($where))) {
				$this->abort("Cannot open folder '{$where}'.");
			}

			// Iterate folder contents.
			while (false !== ( $node = readdir($handle)) )
			{
				// Skip nodes in the ignore list.
				if (in_array($node, $ignore)) {
					continue;
				}

				// Find & replace folder contents, recursively.
				$this->findReplace($find, $replace, $where . DS . $node, $regex, $ignore);
			}

			// Close the folder.
			closedir($handle);

		} else {

			/** @todo Get rid of this else statement. */
			$this->abort("'{$where}' is not a file or directory.");
		}
	}

	/**
	 * Creates a '.gitkeep' file in empty folders inside the given path.
	 *
	 * @param string $path  The path to check for empty folders.
	 * @return void
	 */
	public function gitkeepEmpty($path)
	{
		// Get list of nodes.
		$nodes = $this->scanFolder($path);

		// Is the directory empty?
		if (empty($nodes)) {

			// Create a .gitkeep
			$gitkeep = fopen($path . DS . '.gitkeep', 'w');
			fclose($gitkeep);
		}

		// Iterate directory contents.
		foreach ($nodes as $node) {
			$node = $path . DS . $node;

			// Is a directory?
			if (is_dir($node) === true) {
				$this->gitkeepEmpty($node);
			}
		}
	}

	/**
	 * Aborts the build script. Outputs message to page.
	 *
	 * @param string $message  Abort message to output.
	 * @return void
	 */
	protected function abort($message)
	{
		$output = "Build aborted with message:\n" . $message;
		die($output);
	}

} // BaseBuilder
