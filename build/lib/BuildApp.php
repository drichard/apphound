<?php
/**
 * FnF Application Builder
 *
 * Builds the script from source and assets files writing the compiled code to
 * file. Usage below:
 *
 * Enable cache:
 *     ?cache=all
 * Enable cache by type:
 *     ?cache=files,assets,images
 * List source files to build from cache:
 *     ?cache=files&files=file1,file2,file3
 * Purge temporary files:
 *     ?purge=all
 * Purge temporary files by type:
 *     ?purge=cache,live,release
 * Purge temporary files and die:
 *     ?purge=all,die
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * The BuildApp Class.
 *
 * The BuildApp class is a tool that compiles the framework and application source
 * files and assets into a single file.
 *
 * @package  DigitalPoetry\FnF\Build
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class BuildApp extends BaseBuilder
{
	/**
	 * The cache type to purge before building.
	 *
	 * @var array
	 */
	public $purge_options;

	/**
	 * Indicates if images should be build from cache.
	 *
	 * @var boolean
	 */
	public $use_image_cache;

	/**
	 * Indicates if assets should be build from cache.
	 *
	 * @var boolean
	 */
	public $use_asset_cache;

	/**
	 * Indicates if source should be build from cache. Please note listing files
	 * to build from cache will not work unless this option is set to true.
	 *
	 * @var boolean
	 */
	public $use_file_cache;

	/**
	 * The delimiter string used to cantonate files with. This is in the
	 * configuration. Set in the build configuration.
	 *
	 * @var string
	 */
	protected $file_delimiter;

	/**
	 * Holds the compiled application source code.
	 *
	 * @var array
	 */
	public $source;

	/**
	 * Initialises the build script
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Bring into scope.
		global $config;

		// Set the base builder properties.
		$this->setBaseProperties();

		// Make files array.
		foreach ($this->file_lists as $package => $files)
			$file_lists[$package] = array_flip($files);

		// Set build properties.
		$this->script_path     = BASEPATH . DS . $this->script_path . DS . $this->script_name;
		$this->file_lists      = $file_lists;
		$this->cache_options   = isset($_GET['cache']) ? explode(',', $_GET['cache']) : $this->cache_options;
		$this->purge_options   = isset($_GET['purge']) ? explode(',', $_GET['purge']) : [];
		$this->use_image_cache = in_array('images', $this->cache_options) || in_array('all', $this->cache_options) ? true : false;
		$this->use_asset_cache = in_array('assets', $this->cache_options) || in_array('all', $this->cache_options) ? true : false;
		$this->use_file_cache  = in_array('files', $this->cache_options) || in_array('all', $this->cache_options) ? true : false;
		$this->cache_file_list = isset($_GET['files'])  ? explode(',', $_GET['files']) : $config['build']['default_cache_files'];
		$this->file_delimiter  = $config['build']['file_delimiter'];

		// Purge temporary files?
		$this->purgeTempFiles();
	}

	/**
	 * Compiles the given file sources into a single string of source code.
	 *
	 * @return void
	 */
	public function compile()
	{
		// Build list of source files.
		$this->listSources();

		// Get the source of each file.
		$strip_file_docblocks = false;
		foreach ($this->file_lists as $file_list) {
			foreach ($file_list as $file => $directory) {
				$source = $this->getSource($file, $directory);

				// Don't add empty files.
				if (! empty($source)) {

					// Strip 'file DocBlocks'.
					if ($strip_file_docblocks) {

						// Remove from the start of the file to the end of the
						// first multi-line comment.
						$source = preg_replace('/^.+?\*\//s', '', $source);
					}

					// Trim leading/trailing whitespace.
					$sources[] = trim($source);

					// After first source, use cache and remove file DocBlocks.
					$this->use_asset_cache =
					$this->use_image_cache =
					$strip_file_docblocks  =
					true;
				}
			}
		}

		// Contonate sources and add an opening php tag.
		$this->source = "<?php\n" . implode($this->file_delimiter, $sources) . "\n";
	}

	/**
	 * Gets the source files list.
	 *
	 * Grabs files from any directory listed in the sources array in structure
	 * configuration.
	 *
	 * @return void
	 */
	public function listSources()
	{
		$app_path        = $this->paths['project']['application'];
		$app_paths       = $this->paths['sources']['app'];
		$app_asset_paths = $this->paths['assets']['app'];
		$app_image_paths = $app_path . DS . $this->paths['assets']['app']['img'];
		$modules_path    = $app_path . DS . $app_paths['modules'];
		$sys_paths       = $this->paths['sources']['system'];
		$sys_asset_paths = $this->paths['assets']['theme'];
		$sys_image_paths = $this->paths['assets']['theme']['img'];
		unset($sys_asset_paths['img']);
		unset($app_asset_paths['img']);
		unset($app_paths['modules']);

		// System Sources.
		foreach ($sys_paths as $src_path) {
			foreach ($this->listFiles($src_path) as $file) {
				$this->file_lists['system'][$file] = $src_path;
			}
		}

		// System Assets.
		foreach ($sys_asset_paths as $src_path) {
			foreach ($this->listFiles($src_path) as $file) {
				$this->asset_lists['system'][$file] = $src_path;
			}
		}

		// System Images.
		foreach ($this->listFiles($sys_image_paths) as $file) {
			$this->image_lists['system'][$file] = $sys_image_paths;
		}

		// Application Sources.
		foreach ($app_paths as $src_path) {
			foreach ($this->listFiles($app_path . DS . $src_path) as $file) {
				$this->file_lists['app'][$file] = $app_path . DS . $src_path;
			}
		}

		// Application Assets.
		foreach ($app_asset_paths as $src_path) {
			foreach ($this->listFiles($app_path . DS . $src_path) as $file) {
				$this->asset_lists['app'][$file] = $app_path . DS . $src_path;
			}
		}

		// Application Images.
		foreach ($this->listFiles($app_image_paths) as $file) {
			$this->image_lists['app'][$file] = $app_path . DS . $app_image_paths;
		}

		// Add Modules Recursively.
		foreach ($this->listFolders($modules_path) as $directory) {
			$this->listModuleSources($modules_path . DS . $directory);
		}

		// Add the header file to start of files list.
		$header = $this->file_lists['system']['app_header.php'];
		unset($this->file_lists['system']['app_header.php']);
		$this->file_lists['system'] = array_merge(['app_header.php' => $header], $this->file_lists['system']);

		// Add the file footer to the end of the list.
		$footer = $this->file_lists['system']['app_footer.php'];
		unset($this->file_lists['system']['app_footer.php']);
		$this->file_lists[]['app_footer.php'] = $footer;
	}

	/**
	 * Gets a module's source files list.
	 *
	 * Grabs files from any directory listed in the sources array in structure
	 * configuration.
	 *
	 * @param $module_path Path to the module folder.
	 * @return void
	 */
	public function listModuleSources($module_path)
	{
		$module          = basename($module_path);
		$app_paths       = $this->paths['sources']['app'];
		$app_asset_paths = $this->paths['assets']['app'];
		$app_image_paths = $module_path . DS . $this->paths['assets']['app']['img'];
		$modules_path    = $module_path . DS . $app_paths['modules'];
		unset($app_asset_paths['img']);
		unset($app_paths['modules']);

		// Module Sources.
		foreach ($app_paths as $src_path) {
			foreach ($this->listFiles($module_path . DS . $src_path) as $file) {
				$this->file_lists[$module][$file] = $module_path . DS . $src_path;
			}
		}

		// Module Assets.
		foreach ($app_asset_paths as $src_path) {
			foreach ($this->listFiles($module_path . DS . $src_path) as $file) {
				$this->asset_lists[$module][$file] = $module_path . DS . $src_path;
			}
		}

		// Module Images.
		foreach ($this->listFiles($app_image_paths) as $file) {
			$this->image_lists[$module][$file] = $module_path . DS . $app_image_paths;
		}

		// Add Modules Recursively.
		foreach ($this->listFolders($modules_path) as $directory) {
			$this->listModuleSources($modules_path . DS . $directory);
		}
	}

	/**
	 * Gets the source contents, either raw or from cache. Parses and caches
	 * source files if a cache file is not found.
	 *
	 * @param string  $file  Required. The file to get the contents from.
	 * @param string  $dir   Required. The path to the directory where
	 *                       $file is located.
	 * @return void
	 * @todo Replace application path in cache paths.
	 */
	private function getSource($file, $dir)
	{
		$src_file   = BASEPATH . DS . $dir  . DS . $file;
		$cache_path = BASEPATH . DS . $this->paths['temp']['cache'] . DS . $dir;
		$live_path  = BASEPATH . DS . $this->paths['temp']['live']  . DS . $dir;
		$cache_file = $cache_path . DS . $file . '.txt';
		$live_file  = $live_path  . DS . $file;

		// Use cache?
		if ( $this->use_file_cache && in_array($file, $this->cache_file_list) && file_exists($cache_file) ) {
			$contents = file_get_contents($cache_file);
		} else {
			// Get from source starting at character is 6 to skip php tags.
			$contents = file_get_contents($src_file, null, null, 6);
			// Embed assets first.
			$contents = $this->embedAssets($contents);
			// Embed images second.
			$contents = $this->embedImages($contents);
			// Create a cache and live run file third.
			$this->ensureFolder($cache_path) OR die("Unable to create directory {$cache_path}");
			file_put_contents($cache_file, $contents);
			$this->ensureFolder($live_path) OR die("Unable to create directory {$live_path}");
			file_put_contents($live_file, "<?php\n" . $contents);
		}

		// Is source empty?
		if (! empty($contents)) {
			return $contents;
		}
	}

	/**
	 * Embeds base64 encoded assets into the given source. Caches assets if a
	 * cache file is not found.
	 *
	 * @param string  $source The source code string to embed with assets.
	 * @return string  The source code with assets embeded.
	 */
	private function embedAssets($source)
	{
		// Get the asset file lists.
		$files = $this->asset_lists;

		// Inject each asset in each list.
		foreach ($this->asset_lists as $package => $files_list) {
			foreach ($files_list as $file => $src_path) {
				$marker = '##' . strtoupper($file) . '##';

				// Source contains asset filename?
				if (is_int(strpos($source, $marker))) {
					$asset_url   = $this->url  . DS . $src_path . DS . $file;
					$cache_path  = BASEPATH    . DS . $this->paths['temp']['cache'] . DS . $src_path;
					$cache_file  = $cache_path . DS . $file;

					// Use asset cache?
					if ($this->use_asset_cache && file_exists($cache_file . '.txt')) {
						$encoded = file_get_contents($cache_file . '.txt');
					} else {
						$raw = file_get_contents($asset_url);
						// Inject assets first.
						$raw = $this->injectAssets($raw);
						// Embed images second.
						$raw = $this->embedImages($raw);
						// Cache the raw and encoded asset.
						$this->ensureFolder($cache_path) OR die("Unable to create directory {$cache_path}");
						file_put_contents($cache_file, $raw);
						$encoded = base64_encode($raw);
						file_put_contents($cache_file . '.txt', $encoded);
					}

					// Embed the asset.
					$source = str_replace($marker, $encoded, $source);
				}
			}
		}

		return $source;
	}

	/**
	 * Injects unencoded assets, recursively.
	 *
	 * @param string  $source The source code string to inject with assets.
	 * @return string  The source code with assets injected.
	 */
	private function injectAssets($source)
	{
		// Get the asset file lists.
		$files = $this->asset_lists;

		// Inject each asset in each list.
		foreach ($this->asset_lists as $package => $files_list) {
			foreach ($files_list as $file => $src_path) {
				$marker = '##' . strtoupper($file) . '##';

				// Source contains asset filename?
				if (is_int(strpos($source, $marker))) {
					$asset_url   = $this->url  . DS . $src_path . DS . $file;
					$cache_path  = BASEPATH    . DS . $this->paths['temp']['cache'] . DS . $src_path;
					$cache_file  = $cache_path . DS . $file;

					// Use asset cache?
					if ($this->use_asset_cache && file_exists($cache_file)) {
						$content = file_get_contents($cache_file);
					} else {
						$content = file_get_contents($asset_url);
						// Inject assets recursively first.
						$content = $this->injectAssets($content);
						// Embed images second. 
						// Optional, this will slow the build process down. The 
						// upside is images get embeded in the cached copies of
						// assets that are injected into another asset.
						$content = $this->embedImages($content);
						// Cache the raw asset.
						$this->ensureFolder($cache_path) OR die("Unable to create directory {$cache_path}");
						file_put_contents($cache_file, $content);
					}

					// Inject the asset.
					$source = str_replace($marker, $content, $source);
				}
			}
		}

		return $source;
	}

	/**
	 * Helper fuction that injects bas64 encoded images into the given string.
	 * Caches images if a cache file is not found.
	 *
	 * @param string  $source The source code string to embed with images.
	 * @return string  The source code with images embeded.
	 */
	private function embedImages($source)
	{
		// Get the image file lists.
		$files = $this->image_lists;

		// Inject each image in each list.
		foreach ($this->image_lists as $package => $file_list) {
			foreach ($file_list as $file => $src_path) {
				$marker = '##' . strtoupper($file) . '##';

				// Source contains image filename?
				if (is_int(strpos($source, $marker))) {
					$cache_path  = BASEPATH    . DS . $this->paths['temp']['cache'] . DS . $src_path;
					$cache_file  = $cache_path . DS . $file . '.txt';
					$src_file    = BASEPATH    . DS . $src_path . DS . $file;

					// Use image cache?
					if ($this->use_image_cache && file_exists($cache_file)) {
						$encoded = file_get_contents($cache_file);
					} else {
						$encoded = base64_encode(file_get_contents($src_file));
						// Cache the encoded image.
						$this->ensureFolder($cache_path) OR die("Unable to create directory {$cache_path}");
						file_put_contents($cache_file, $encoded);
					}

					// Embed the image.
					$source = str_replace($marker, $encoded, $source);
				}
			}
		}

		return $source;
	}

	/**
	 * Purges the build cache files.
	 *
	 * @return void
	 */
	private function purgeTempFiles()
	{
		// Purge options.
		$options = $this->purge_options;

		// Purge all option.
		$purge_all = in_array('all', $options);

		// Purge cache files?
		if (in_array('cache', $options) || $purge_all) {
			$this->cleanFolder(BASEPATH . DS . $this->paths['temp']['cache']);
		}

		// Purge live files?
		if (in_array('live', $options) || $purge_all) {
			$this->cleanFolder(BASEPATH . DS . $this->paths['temp']['live']);
		}

		// Purge release files?
		if (in_array('release', $options) || $purge_all) {
			$this->cleanFolder(BASEPATH . DS . $this->paths['temp']['release']);
		}

		// Die after purge?
		if (in_array('die', $options)) {
			die('Purging temporary files.');
		}
	}

	/**
	 * Writes the compiled code to file.
	 *
	 * @return void
	 */
	public function writeScript()
	{
		file_put_contents($this->script_path, $this->source);
	}

} // BuildApp
