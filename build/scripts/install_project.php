<?php
/**
 * FnF Project Install Script
 *
 * Installs the project.
 *
 * @todo Rename this script *setup*.php or w/e...
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Instantiate the project builder.
$project_builder = new BuildProject();

// Set the install path.
$path = $project_builder->paths['sources']['system']['config'] . DS . 'config.php';

// Generate a salt.
$length = 100;
$find = '/\[[\'\"]secure_salt[\'\"]\] = [\'\"].+?[\'\"];/';
$replace = "['secure_salt'] = '" . $project_builder->generateSalt($length) . "';";

// Inject the salt into the source file.
$project_builder->findReplace($find, $replace, $path, true);
