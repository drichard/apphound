<?php
/**
 * FnF Generate Documentation Script
 *
 * Execute a shell command to commit to a git repository.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/*
 * phpDoc Reference
 *
 * Usage:
 *   phpdoc project:run [options]
 *   phpdoc run
 *
 * Options:
 *   -t, --target[=TARGET]            Path where to store the generated output.
 *       --cache-folder[=PATH]        Path where to store the cache files.
 *   -f, --filename[=PATH]            Comma-separated list of files to parse. The wildcards ? and * are supported (multiple values allowed).
 *   -d, --directory[=PATH]           Comma-separated list of directories to (recursively) parse (multiple values allowed).
 *       --encoding[=ENCODING]        Encoding to be used to interpret source files with.
 *   -e, --extensions[=EXTS]          Comma-separated list of extensions to parse, defaults to php, php3 and phtml (multiple values allowed).
 *   -i, --ignore[=IGNORE]            Comma-separated list of file(s) and directories (relative to the source-code directory) that will be ignored. Wildcards * and ? are supported (multiple values allowed).
 *       --ignore-tags[=TAGS]         Comma-separated list of tags that will be ignored, defaults to none. package, subpackage and ignore may not be ignored. (multiple values allowed).
 *       --hidden                     Use this option to tell phpDocumentor to parse files and directories that begin with a period (.), by default these are ignored.
 *       --ignore-symlinks            Ignore symlinks to other files or directories, default is on.
 *   -m, --markers[=MARKERS]          Comma-separated list of markers/tags to filter (multiple values allowed).
 *       --title[=TITLE]              Sets the title for this project; default is the phpDocumentor logo.
 *       --force                      Forces a full build of the documentation, does not increment existing documentation.
 *       --validate                   Validates every processed file using PHP Lint, costs a lot of performance.
 *       --visibility[=VISIBILITY]    Specifies the parse visibility that should be displayed in the documentation (comma separated e.g. "public,protected") (multiple values allowed).
 *       --defaultpackagename[=NAME]  Name to use for the default package. [default: "Default"].
 *       --sourcecode                 Whether to include syntax highlighted source code.
 *   -p, --progressbar                Whether to show a progress bar; will automatically quiet logging to stdout.
 *       --template[=TEMPLATE]        Name of the template to use (optional) (multiple values allowed).
 *       --parseprivate               Whether to parse DocBlocks marked with @internal tag.
 *       --log[=LOG]                  Log file to write to.
 *   -h, --help                       Display this help message.
 *   -q, --quiet                      Do not output any message.
 *   -V, --version                    Display this application version.
 *       --ansi                       Force ANSI output.
 *       --no-ansi                    Disable ANSI output.
 *   -n, --no-interaction             Do not ask any interactive question.
 *   -c, --config[=CONFIG]            Location of a custom configuration file.
 *   -v|vv|vvv, --verbose             Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug.
 *
 * Usage:
 *   phpdoc project:transform [options]
 *   phpdoc transform
 *
 * Options:
 *   -s, --source[=SOURCE]      Path where the XML source file is located (optional).
 *   -t, --target[=TARGET]      Path where to store the generated output (optional).
 *       --template[=TEMPLATE]  Name of the template to use (optional) (multiple values allowed).
 *   -p, --progressbar          Whether to show a progress bar; will automatically quiet logging to stdout.
 *       --log[=LOG]            Log file to write to.
 *   -h, --help                 Display this help message.
 *   -q, --quiet                Do not output any message.
 *   -V, --version              Display this application version.
 *       --ansi                 Force ANSI output.
 *       --no-ansi              Disable ANSI output.
 *   -n, --no-interaction       Do not ask any interactive question.
 *   -c, --config[=CONFIG]      Location of a custom configuration file.
 */

/*
Build all themes:
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\abstract --template abstract
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\clean --template clean
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\new-black --template new-black
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\old-ocean --template old-ocean
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\responsive --template responsive
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\responsive-twig --template responsive-twig
phpdoc run --sourcecode --parseprivate -c phpdoc.dist.xml -t docs\_themes\zend --template zend
*/

/** @todo Delete Me */
require_once dirname(__DIR__) . '/bootstrap.php';

// Bring $config into scope.
global $config;

// OS Name.
$exec = (stripos(php_uname('s'), 'windows') === false) ? 'shell_exec' : 'exec';
// Path to vendor bin.
$vendor_bin = BASEPATH . DS . $config['paths']['project']['vendor'] . DS . 'bin';
// Path to config.
$conf = BASEPATH . DS . 'phpdoc.dist.xml';
// Path to output.
$target = BASEPATH . DS . $config['paths']['project']['docs'];
// Path to cache.
$cache = BASEPATH . DS . $config['paths']['temp']['docs'];
// Path to log.
$log = BASEPATH . DS . $config['paths']['temp']['logs'] . DS . 'phpdoc_build_log.txt';
// Log header.
$log_header =
str_repeat('=', 41) .
"\nDocumentation Build " . date("n/j/y h:i:s A") . "\n" .
str_repeat('-', 41) . "\n";
// Path to Template.
$template = $target . DS . '.data' . DS . 'templates' . DS . 'responsive';
// Shell command options.
$options =
" --config {$conf}" .
" --target {$target}" .
" --cache-folder {$cache}" .
/*" --log {$log}" .*/
" --template {$template}" .
" --sourcecode" .
" --parseprivate" .
" --validate" .
" --force";
$mode = (isset($_GET['append_log'])) ? FILE_APPEND : null;

// Generate docs.
$stout = shell_exec($vendor_bin . DS . 'phpdoc run' . $options);

// Write logs to file.
$log_contents = $log_header . $stout . "\n";
file_put_contents($log, $log_contents, $mode);

// Output.
#print_r($log_contents);
