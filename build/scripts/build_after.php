<?php
/**
 * FnF Application Post-build Script
 *
 * This script is ran after each build.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// TODO: Use this file or remove it.

// Run unit tests
// TODO:  Run unit tests

$phpdoc = 'silence build errors';
